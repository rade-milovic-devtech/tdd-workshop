package com.devtechgroup.tddworkshop.stringcalculator;

import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;
import static java.util.Arrays.stream;

public class StringCalculator {

    private final String customDelimiterPrefix = "//";
    private final String negativeNumbersPrefix = "-";
    private final String commaDelimiter = ",";
    private final String newLineDelimiter = "\\r?\\n";
    private final String commaOrNewLineDelimiter = ",|\\r?\\n";

    public int add(String input) {
        if(input.isEmpty()) {
            return 0;
        }
        else {
            String delimiter = hasCustomDelimiter(input) ? getCustomDelimiter(input) : commaOrNewLineDelimiter;
            String numbersSequence = hasCustomDelimiter(input) ? getNumbersSequence(input) : input;

            String[] numbers = numbersSequence.split(delimiter);

            if (hasMultipleNumbers(numbers)) {
                checkForNegativeNumbers(numbers);

                return calculateSum(numbers);
            }
            else {
                checkForNegativeNumber(input);

                return parseInt(input);
            }
        }
    }

    private boolean hasCustomDelimiter(String input) {
        return input.startsWith("//");
    }

    private String getCustomDelimiter(String input) {
        return splitCustomDelimiterAndNumbersSequence(input)[0].replace(customDelimiterPrefix, "");
    }

    private String getNumbersSequence(String input) {
        return splitCustomDelimiterAndNumbersSequence(input)[1];
    }

    private boolean hasMultipleNumbers(String[] numbers) {
        return numbers.length > 1;
    }

    private void checkForNegativeNumbers(String[] numbers) {
        String negativeNumbers = stream(numbers)
            .filter(this::isNegativeNumber)
            .collect(Collectors.joining(commaDelimiter));

        if (!negativeNumbers.isEmpty()) {
            throw new NegativesNotAllowedException("Negative numbers found: " + negativeNumbers);
        }
    }

    private int calculateSum(String[] numbers) {
        return stream(numbers)
                .mapToInt(Integer::parseInt)
                .sum();
    }

    private void checkForNegativeNumber(String number) {
        if (isNegativeNumber(number)) {
            throw new NegativesNotAllowedException("Negative numbers found: " + number);
        }
    }

    private String[] splitCustomDelimiterAndNumbersSequence(String input) {
        return input.split(newLineDelimiter);
    }

    private boolean isNegativeNumber(String number) {
        return number.startsWith(negativeNumbersPrefix);
    }
}
