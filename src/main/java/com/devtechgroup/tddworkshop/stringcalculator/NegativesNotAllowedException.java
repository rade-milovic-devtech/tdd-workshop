package com.devtechgroup.tddworkshop.stringcalculator;

public class NegativesNotAllowedException extends RuntimeException {

    public NegativesNotAllowedException(String message) {
        super(message);
    }
}
