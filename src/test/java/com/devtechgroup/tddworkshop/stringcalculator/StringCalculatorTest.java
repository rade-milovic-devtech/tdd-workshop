package com.devtechgroup.tddworkshop.stringcalculator;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class StringCalculatorTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private final StringCalculator stringCalculator = new StringCalculator();

    private String input;
    private int expectedResult;
    private int result;

    @Test
    public void add_forEmptyInput_returnsZero() {
        result = stringCalculator.add("");

        assertEquals(0, result);
    }

    @Test
    @Parameters({ "1, 1", "2, 2" })
    public void add_forInputWithOneNumber_returnsSameNumber(String input, int expectedResult) {
        result = stringCalculator.add(input);

        assertEquals(expectedResult, result);
    }

    @Test
    @Parameters(method = "sumOfTwoNumbersData")
    public void add_forInputWithTwoNumbers_returnsSumOfTwoNumbers(String input, int expectedResult) {
        result = stringCalculator.add(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void add_forInputWithArbitraryAmountOfNumbers_returnsSumOfThoseNumbers() {
        input = "1,2,3";
        expectedResult = 6;

        result = stringCalculator.add(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void add_forNewLineNumbersDelimiter_returnsSumOfThoseNumbers() {
        input = "1\n2,3";
        expectedResult = 6;

        result = stringCalculator.add(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void add_forCustomNumbersDelimiter_returnsSumOfThoseNumbers() {
        input = "//;\n1;2";
        expectedResult = 3;

        result = stringCalculator.add(input);

        assertEquals(expectedResult, result);
    }

    @Test
    @Parameters(method = "negativeNumbersData")
    public void add_forInputWithNegativeNumbers_throwsExceptionContainingNegativeNumbers(String input, String expectedNegativeNumbers) {
        exception.expect(NegativesNotAllowedException.class);
        exception.expectMessage(expectedNegativeNumbers);

        stringCalculator.add(input);
    }

    private Object sumOfTwoNumbersData() {
        return new Object[] {
                new Object[] { "1,2", 3 },
                new Object[] { "2,3", 5 }
        };
    }

    private Object negativeNumbersData() {
        return new Object[] {
                new Object[] { "1,-2,-3,4", "-2,-3" },
                new Object[] { "-1", "-1" },
                new Object[] { "1\n-2\n-3,4", "-2,-3" },
                new Object[] { "//;\n1;-2;-3;4", "-2,-3" },
        };
    }
}
